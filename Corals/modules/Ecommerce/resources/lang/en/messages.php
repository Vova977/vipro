<?php
return [
    'order' => [
        'notify_buyer_confirmation' => 'You are going to notify the buyer with updates',
        'buyer_notified_successfully' => 'Buyer notification has been sent successfully',
        'order_placed' => 'Order has been successfully placed',
    ],
    'refund' => [
        'do_refund_order' => 'Refund from Order has been successfully'
    ],
    'validation' => [
        'greater_than_zero' => 'The amount field it must greater than zero'
    ]
];
