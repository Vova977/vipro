<?php

return [
    'order' => [
        'pending' => 'Pending',
        'processing' => 'Processing',
        'submitted' => 'Submitted',
        'canceled' => 'Canceled',
        'completed' => 'Completed'
    ],
    'shipping' => [
        'pending' => 'Pending'
        , 'shipped' => 'Shipped'
        , 'in_progress' => 'InProgress'
    ],
    'payment' => [
        'pending' => 'Pending',
        'paid' => 'Paid',
        'canceled' => 'Canceled',
        'refunded' => 'Refunded',
        'partial_refund' => 'Partial Refund'
    ],
    'shop_order' => [
        'low_high_price' => 'Low - High Price',
        'high_low_price' => 'High - Low Price',
        'a_z_order' => 'A - Z Order',
        'z_a_order' => 'Z - A Order'
    ]
];