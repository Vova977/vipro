<?php

namespace Corals\Modules\Ecommerce\Policies;

use Corals\User\Models\User;
use Corals\Modules\Ecommerce\Models\Order;

class OrderPolicy
{

    /**
     * @param User $user
     * @param null $order
     * @return bool
     */
    public function view(User $user, $order = null)
    {
        if ($user->can('Ecommerce::orders.access')) {
            return true;
        }

        if ($user->can('Ecommerce::my_orders.access') && $order && $order->user->id == $user->id) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user)
    {
        return $user->can('Ecommerce::order.create');
    }

    /**
     * @param User $user
     * @param Order $order
     * @return bool
     */
    public function update(User $user, Order $order)
    {
        if ($user->can('Ecommerce::order.update') && $order->status == 'pending') {
            return true;
        }
        return false;
    }

    /**
     * @param User $user
     * @param Order $order
     * @return bool
     */
    public function destroy(User $user, Order $order)
    {
        if ($user->can('Ecommerce::order.delete')) {
            return true;
        }
        return false;
    }

    public function payOrder(User $user, Order $order)
    {
        return $user->hasPermissionTo('Ecommerce::my_orders.access') && $order->user->id == $user->id && ($order->status == "pending");
    }


    /**
     * @param $user
     * @param $ability
     * @return bool
     */
    public function before($user, $ability)
    {
        $skipAbilities = ['payOrder'];

        if ($user->hasPermissionTo('Administrations::admin.ecommerce') && !in_array($ability, $skipAbilities)) {
            return true;
        }

        return null;
    }
}
