<?php

namespace Corals\Modules\Ecommerce\Transformers;

use Corals\Foundation\Transformers\BaseTransformer;
use Corals\Modules\Ecommerce\Models\Order;

class OrderTransformer extends BaseTransformer
{
    public function __construct()
    {
        $this->resource_url = config('ecommerce.models.order.resource_url');

        parent::__construct();
    }

    /**
     * @param Order $order
     * @return array
     * @throws \Throwable
     */
    public function transform(Order $order)
    {
        $actions = ['delete' => ''];

        $levels = [
            'pending' => 'info',
            'submitted' => 'info',
            'processing' => 'success',
            'completed' => 'primary',
            'failed' => 'danger',
            'canceled' => 'warning'
        ];

        $payment_levels = [
            'pending' => 'info',
            'paid' => 'success',
            'canceled' => 'danger',
            'refunded' => 'danger',
            'partial_refund' => 'warning'
        ];

        $payment_status = $order->billing['payment_status'] ?? '';
        $user_id = null;
        if (user()) {
            if (user()->can('Ecommerce::order.update')) {
                $actions['update_payment'] = [
                    'icon' => 'fa fa-fw fa-money',
                    'href' => url($this->resource_url . '/' . $order->hashed_id . '/edit-payment'),
                    'label' => trans('Ecommerce::labels.order.update_payment'),
                    'class' => 'modal-load',
                    'data' => [
                        'title' => trans('Ecommerce::labels.order.update_payment')
                    ]
                ];
                $actions['update_shipping'] = [
                    'icon' => 'fa fa-fw fa-truck',
                    'href' => url($this->resource_url . '/' . $order->hashed_id . '/edit-shipping'),
                    'label' => trans('Ecommerce::labels.order.update_shipping'),
                    'class' => 'modal-load',
                    'data' => [
                        'title' => trans('Ecommerce::labels.order.update_shipping')
                    ]
                ];

                $actions['update_status'] = [
                    'icon' => 'fa fa-fw fa-flag',
                    'href' => url($this->resource_url . '/' . $order->hashed_id . '/edit-status'),
                    'label' => trans('Ecommerce::labels.order.update_status'),
                    'class' => 'modal-load',
                    'data' => [
                        'title' => trans('Ecommerce::labels.order.update_status')
                    ]
                ];

                $actions['notify_buyer'] = [
                    'icon' => 'fa fa-fw fa-bell',
                    'href' => url($this->resource_url . '/' . $order->hashed_id . '/notify-buyer'),
                    'label' => trans('Ecommerce::labels.order.notify_buyer'),
                    'data' => [
                        'action' => 'post',
                        'confirmation' => trans('Ecommerce::messages.order.notify_buyer_confirmation'),
                    ]
                ];
                if ($payment_status && $payment_status != 'refunded' && $order->status != 'canceled') {
                    $actions['refund_order'] = [
                        'icon' => 'fa fa-undo',
                        'href' => url($this->resource_url . '/' . $order->hashed_id . '/refund-order'),
                        'label' => trans('Ecommerce::labels.order.refund_order'),
                        'class' => 'modal-load',
                        'data' => [
                            'title' => trans('Ecommerce::labels.order.refund_order')
                        ]
                    ];
                }
            }
            if ($order->user) {
                if (user()->can('payOrder', $order) && $payment_status != 'paid') {
                    $actions['pay_order'] = [
                        'icon' => 'fa fa-fw fa-edit',
                        'href' => url('e-commerce/checkout/?order=' . $order->hashed_id),
                        'label' => trans('Ecommerce::labels.order.pay_order'),
                        'data' => [
                        ]

                    ];
                }
            }


            if ($order->status != "pending") {
                $actions['edit'] = "";
            }


        }


        if ($order->user) {
            if (user() && user()->can('view', $order->user)) {
                $user_id = "<a target='_blank' href='" . url('users/' . $order->user->hashed_id) . "'> {$order->user->full_name}</a>";

            } else {
                $user_id = $order->user->full_name;
            }
        } else {
            $user_id = trans('Ecommerce::labels.order.guest');
        }

        $currency = strtoupper($order->currency);

        $transformedArray = [
            'status' => formatStatusAsLabels($order->status, ['level' => $levels[$order->status], 'text' => trans('Ecommerce::status.order.' . $order->status)]),

            'order_number' => '<a href="' . url($this->resource_url . '/' . $order->hashed_id) . '">' . $order->order_number . '</a>',
            'id' => $order->id,
            'checkbox' => $this->generateCheckboxElement($order),
            'currency' => $currency,
            'amount' => \Payments::currency_convert($order->amount, null, $currency, true),
            'user_id' => $user_id,
            'payment_status' => $payment_status ? formatStatusAsLabels($payment_status, ['level' => $payment_levels[$payment_status], 'text' => trans('Ecommerce::status.payment.' . $payment_status)]) : ' -  ',
            'created_at' => format_date($order->created_at),
            'updated_at' => format_date($order->updated_at),
            'action' => $this->actions($order, $actions)
        ];

        return parent::transformResponse($transformedArray);
    }
}
