<?php

namespace Corals\Modules\Ecommerce\Charts;

use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class BrandRatio extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
}
