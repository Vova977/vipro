<?php

namespace Corals\Modules\Ecommerce\Providers;

use Corals\Modules\Ecommerce\Models\Order;
use Corals\Modules\Ecommerce\Models\Product;
use Corals\Modules\Ecommerce\Models\SKU;
use Corals\Modules\Ecommerce\Policies\OrderPolicy;
use Corals\Modules\Ecommerce\Policies\ProductPolicy;
use Corals\Modules\Ecommerce\Policies\SKUPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class EcommerceAuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Product::class => ProductPolicy::class,
        SKU::class => SKUPolicy::class,
        Order::class => OrderPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}
