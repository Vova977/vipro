<?php

namespace Corals\Modules\Payment\Common\Transformers;

use Corals\Foundation\Transformers\FractalPresenter;

class InvoiceItemPresenter extends FractalPresenter
{

    /**
     * @return InvoiceItemTransformer|\League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new InvoiceItemTransformer();
    }
}